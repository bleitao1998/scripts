﻿using System;
using System.Collections.Generic;
using Excel  = Microsoft.Office.Interop.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Linq;

namespace NormalizeExcel
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] files = { "pedal", "topCover", "housing"};

            foreach (var file in files)
            {
                Console.WriteLine(file + ".xlsx - Start");

                string path = AppDomain.CurrentDomain.BaseDirectory + file;

                Dictionary<int, Dictionary<string, List<logObject>>> fullObject = ReadFromExcelFile(path + ".xlsx");

                List<logObject> normalizedExcel = normalizeRows(fullObject);

                writeOnExcel(normalizedExcel, path + "_normalized.xlsx");

                Console.WriteLine(file + ".xlsx - Done");
            }

            Console.WriteLine("Press any key to continue");

            Console.ReadLine();            
        }

        private static void writeOnExcel(List<logObject> normalizedExcel, string path)
        {
            Excel.Application excelAplication = new Excel.Application();
            excelAplication.Visible = false;

            //opens the excel file
            Excel.Workbook excelWorkbook = excelAplication.Workbooks.Add();
            Excel.Worksheet excelWorksheet = (Excel.Worksheet)excelWorkbook.Worksheets.get_Item(1);

            excelWorksheet.Cells[1, 1] = "molde";
            excelWorksheet.Cells[1, 2] = "case";
            excelWorksheet.Cells[1, 3] = "cavities";
            excelWorksheet.Cells[1, 4] = "material_type";
            excelWorksheet.Cells[1, 5] = "molde_area";
            excelWorksheet.Cells[1, 6] = "costs";
            excelWorksheet.Cells[1, 7] = "complexity";
            excelWorksheet.Cells[1, 8] = "subcontracting";
            excelWorksheet.Cells[1, 9] = "num_peca";
            excelWorksheet.Cells[1, 10] = "start_timestamp";
            excelWorksheet.Cells[1, 11] = "time:timestamp";
            excelWorksheet.Cells[1, 12] = "activity";
            excelWorksheet.Cells[1, 13] = "org:resource";

            for (int i = 0; i < normalizedExcel.Count; i++)
            {
                excelWorksheet.Cells[i+2, 1] = normalizedExcel[i].molde;
                excelWorksheet.Cells[i+2, 2] = normalizedExcel[i].caseId;
                excelWorksheet.Cells[i+2, 3] = normalizedExcel[i].cavities;
                excelWorksheet.Cells[i+2, 4] = normalizedExcel[i].material_type;
                excelWorksheet.Cells[i+2, 5] = normalizedExcel[i].molde_area;
                excelWorksheet.Cells[i+2, 6] = normalizedExcel[i].costs;
                excelWorksheet.Cells[i+2, 7] = normalizedExcel[i].complexity;
                excelWorksheet.Cells[i+2, 8] = normalizedExcel[i].subcontracting;
                excelWorksheet.Cells[i+2, 9] = normalizedExcel[i].num_peca;
                excelWorksheet.Cells[i+2, 10] = normalizedExcel[i].start_timestamp.ToString("yyyy-MM-dd HH:mm:ss");
                excelWorksheet.Cells[i+2, 11] = normalizedExcel[i].timeTimestamp.ToString("yyyy-MM-dd HH:mm:ss");
                excelWorksheet.Cells[i+2, 12] = normalizedExcel[i].activity;
                excelWorksheet.Cells[i+2, 13] = normalizedExcel[i].resource;
            }

            excelWorkbook.SaveAs(path);
            Console.WriteLine("Excel Saved");

            excelWorkbook.Close();
            excelAplication.Quit();

            ReleaseCOMObjects(excelWorkbook);
            ReleaseCOMObjects(excelAplication);
            ReleaseCOMObjects(excelWorksheet);            
        }

        private static List<logObject> normalizeRows(Dictionary<int, Dictionary<string, List<logObject>>> fullObject)
        {
            List<logObject> objects = new List<logObject>();

            foreach (var key in fullObject.Keys)
            {
                foreach (var key2 in fullObject[key].Keys)
                {
                    int i = 0;
                    double duration = 0;
                    DateTime startTime = new DateTime();

                    foreach (logObject item in fullObject[key][key2])
                    {
                        
                        if (fullObject[key][key2].Count - 1 != i && item.activity == fullObject[key][key2][i + 1].activity)
                        {
                            duration += (item.timeTimestamp - item.start_timestamp).TotalSeconds;
                            if (i == 0 || fullObject[key][key2][i - 1].activity != item.activity)
                            {
                                startTime = item.start_timestamp;
                            }
                        }
                        else
                        {
                            if (i == 0 || fullObject[key][key2][i - 1].activity != item.activity)
                            {
                                objects.Add(item);
                            }
                            else
                            {
                                duration += (item.timeTimestamp - item.start_timestamp).TotalSeconds;

                                logObject obj = new logObject()
                                {
                                    molde = item.molde,
                                    caseId = item.caseId,
                                    cavities = item.cavities,
                                    subcontracting = item.subcontracting,
                                    num_peca = item.num_peca,
                                    costs = item.costs,
                                    activity = item.activity,
                                    complexity = item.complexity,
                                    molde_area = item.molde_area,
                                    material_type = item.material_type,
                                    start_timestamp = startTime,
                                    timeTimestamp = startTime.AddSeconds(duration),
                                    resource = item.resource
                                };

                                objects.Add(obj);
                            }
                        }

                        i++;
                    }
                }
            }

            Console.WriteLine("Normalize Completed");

            return objects;
        }

        public static Dictionary<int, Dictionary<string, List<logObject>>> ReadFromExcelFile(string filename)
        {
            var excelAplication = new Excel.Application();
            excelAplication.Visible = false;

            Dictionary<int, Dictionary<string, List<logObject>>> objectsDic = new Dictionary<int, Dictionary<string, List<logObject>>>();

            var excelWorkbook = excelAplication.Workbooks.Open(filename);
            var excelWorksheet = (Excel.Worksheet)excelWorkbook.ActiveSheet;
            int countLines = 0;
            for (int i = 2; i < excelWorksheet.UsedRange.Rows.Count; i++)
            {
                logObject obj = new logObject();
                
                obj.cavities = Convert.ToString(excelWorksheet.Cells[i, 3].Value);
                obj.material_type = Convert.ToString(excelWorksheet.Cells[i, 4].Value);
                obj.molde_area = Convert.ToString(excelWorksheet.Cells[i, 5].Value);
                obj.costs = float.Parse(Convert.ToString(excelWorksheet.Cells[i, 6].Value));
                obj.complexity = int.Parse(Convert.ToString(excelWorksheet.Cells[i, 7].Value));
                obj.subcontracting = int.Parse(Convert.ToString(excelWorksheet.Cells[i, 8].Value));
                obj.num_peca = Convert.ToString(excelWorksheet.Cells[i, 9].Value);

                obj.molde = int.Parse(Convert.ToString(excelWorksheet.Cells[i, 1].Value));
                obj.caseId = int.Parse(Convert.ToString(excelWorksheet.Cells[i, 2].Value));

                obj.start_timestamp = Convert.ToDateTime(Convert.ToString(excelWorksheet.Cells[i, 10].Value));
                obj.timeTimestamp = Convert.ToDateTime(Convert.ToString(excelWorksheet.Cells[i, 11].Value));
                obj.activity = Convert.ToString(excelWorksheet.Cells[i, 12].Value);
                obj.resource = Convert.ToString(excelWorksheet.Cells[i, 13].Value);

                if (objectsDic.ContainsKey(obj.caseId))
                {
                    if (objectsDic[obj.caseId].ContainsKey(obj.num_peca))
                    {
                        objectsDic[obj.caseId][obj.num_peca].Add(obj);
                        continue;
                    }
                    else
                    {
                        objectsDic[obj.caseId].Add(obj.num_peca, new List<logObject>());
                        objectsDic[obj.caseId][obj.num_peca].Add(obj);
                        continue;
                    }
                    
                }

                objectsDic.Add(obj.caseId, new Dictionary<string, List<logObject>>());
                objectsDic[obj.caseId].Add(obj.num_peca, new List<logObject>());
                objectsDic[obj.caseId][obj.num_peca].Add(obj);
            }

            excelWorkbook.Close();
            excelAplication.Quit();

            ReleaseCOMObjects(excelWorkbook);
            ReleaseCOMObjects(excelAplication);
            ReleaseCOMObjects(excelWorksheet);

            Console.WriteLine("Excel Exported");

            return objectsDic;
        }

        public static void ReleaseCOMObjects(Object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception release COM object." + ex.ToString());
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
