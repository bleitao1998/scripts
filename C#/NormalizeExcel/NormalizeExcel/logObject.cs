﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NormalizeExcel
{
    public class logObject
    {
        public int molde { get; set; }
        public int caseId { get; set; }
        public string cavities { get; set; }
        public string material_type{ get; set; }
        public string molde_area{ get; set; }
        public float costs{ get; set; }
        public int complexity{ get; set; }
        public int subcontracting { get; set; }
        public string num_peca { get; set; }
        public DateTime start_timestamp { get; set; }
        public DateTime timeTimestamp { get; set; }
        public string activity { get; set; }
        public string resource { get; set; }

        public logObject(int molde, int caseId, string cavities, string num_peca, string material_type, string molde_area, float costs, int complexity, int subcontracting, DateTime start_timestamp, DateTime timeTimestamp, string activity, string resource)
        {
            this.molde = molde;
            this.caseId = caseId;
            this.cavities = cavities;
            this.material_type = material_type;
            this.molde_area = molde_area;
            this.costs = costs;
            this.complexity = complexity;
            this.subcontracting = subcontracting;
            this.num_peca = num_peca;
            this.start_timestamp = start_timestamp;
            this.timeTimestamp = timeTimestamp;
            this.activity = activity;
            this.resource = resource;
        }

        public logObject()
        {
        }
    }
}
