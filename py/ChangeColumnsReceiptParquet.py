import pandas as pd
parquet = pd.read_parquet(r'C:\Users\b_ml_\Documents\ProjetoEI\pm4py\files\event_logs\receipt.parquet')
df = parquet.rename(columns={
    'caseAAAchannel' : 'case:channel', 
    'caseAAAconceptAAAname' : 'case:concept:name', 
    'caseAAAdeadline' : 'case:deadline', 
    'caseAAAdepartment' : 'case:department', 
    'caseAAAenddate' : 'case:enddate', 
    'caseAAAenddate_planned' : 'case:enddate_planned', 
    'caseAAAgroup' : 'case:group', 
    'caseAAAresponsible' : 'case:responsible', 
    'caseAAAstartdate' : 'start_timestamp', 
    'conceptAAAinstance' : 'concept:instance', 
    'conceptAAAname' : 'concept:name', 
    'lifecycleAAAtransition' : 'lifecycle:transition', 
    'orgAAAgroup' : 'org:group', 
    'orgAAAresource' : 'org:resource', 
    'timeAAAtimestamp' : 'time:timestamp'
})
df.to_parquet(r'C:\Users\b_ml_\Desktop\receipt.parquet') 
