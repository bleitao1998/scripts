Sub AddUser()
 Dim totalRows As Long
 
 totalRows = ActiveSheet.UsedRange.Rows.Count
 
    For i = 2 To totalRows
        If Cells(i, 12).Value = 0 Then
            Cells(i, 12).Value = "No User"
        Else
            Cells(i, 12).Value = "User " & Cells(i, 12).Value
        End If
    Next i
    
End Sub