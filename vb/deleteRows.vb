Sub Delete()
    Dim rowsNumber As Long
    Dim savePosition As Long
    Dim visibleRows As Long
    Dim pointerVisibleRows As Long
    
    '2. Delete Rows
    Application.DisplayAlerts = False
     rowsNumber = ActiveSheet.UsedRange.Rows.Count
     savePosition = 1
     pointerVisibleRows = 0
     With ActiveSheet
        visibleRows = .AutoFilter.Range.Columns(11).SpecialCells(xlCellTypeVisible).Count - 1
     End With
      For i = 2 To rowsNumber
        If Rows(i).EntireRow.Hidden Then
            If visibleRows = pointerVisibleRows Then
                Exit For
            End If
        ElseIf Cells(i, 11) = Null Then
            Exit For
        ElseIf StrComp("", Cells(i, 11)) = 0 Then
            Exit For
        ElseIf StrComp(Cells(savePosition, 11), Cells(i, 11)) = 0 Then
          Rows(i).EntireRow.Delete
          i = i - 1
          pointerVisibleRows = pointerVisibleRows + 1
        ElseIf Cells(savePosition, 11) <> Cells(i, 11) Then
            savePosition = i
            pointerVisibleRows = pointerVisibleRows + 1
        End If
      Next i
      
    Application.DisplayAlerts = True
End Sub